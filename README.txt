CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Opportunities Widget allows the user to incorporate smart self-updating list
of academic conferences, scholarships, or vacancies into the web-site.

The user can configure and display these types of opportunities:
 * academic conferences; you can specify scientific area
 * grants/scholarships; you can select their type
 * scientific vacancies; you can choose their area

Additionally user can configure the language and the quantity of opportunities
being displayed.

 * For a full description of the module visit:
   https://www.drupal.org/project/sci_opportunities_widget

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/sci_opportunities_widget


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install the Opportunities Widget module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information.


CONFIGURATION
-------------

    1. Navigate to Administration > Extend and enable the module.
    2. Navigate to Administration > Configuration > Web Services > SSC widget
       Config for the form for the Scientific Opportunities widget.
    3. Select opportunity type to display in widget: conferences, grants, or
       vacancies.
    4. Select the Scientific area of conferences.
    5. Select the language of opportunities to display.
    6. Enter the number of items to display in widget.
    7. Save configuration.


MAINTAINERS
-----------

 * Ievgen Melezhyk (melezhik_ea) - https://www.drupal.org/u/melezhik_ea

Widget is provided by Science-Community.org project:

 * https://www.science-community.org/en